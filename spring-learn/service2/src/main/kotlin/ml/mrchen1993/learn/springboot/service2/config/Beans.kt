package ml.mrchen1993.learn.springboot.service2.config

import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class Beans{
    @Bean
    fun restTemplate(builder:RestTemplateBuilder):RestTemplate = builder.build()
}