package ml.mrchen1993.learn.springboot.service2.converter

import ml.mrchen1993.learn.springboot.service2.utils.DATE_PATTERN
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import java.sql.Date
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.persistence.AttributeConverter
import javax.persistence.Converter as JPAConverter

@JPAConverter
class LocalDate2Date : AttributeConverter<LocalDate, Date> {
    override fun convertToDatabaseColumn(attribute: LocalDate?): Date? {
        return if (attribute != null) Date.valueOf(attribute) else null
    }

    override fun convertToEntityAttribute(dbData: Date?): LocalDate? {
        return dbData?.toLocalDate()
    }
}


@Component
class String2LocalDate : Converter<String, LocalDate> {
    override fun convert(source: String): LocalDate? {
        if (StringUtils.isEmpty(source)) {
            return null
        }
        val formatter = DateTimeFormatter.ofPattern(DATE_PATTERN)
        return LocalDate.parse(source, formatter)
    }
}
