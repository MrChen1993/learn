package ml.mrchen1993.learn.springboot.service2.config.security.filter

import ml.mrchen1993.learn.springboot.service2.config.security.token.PreAuthenticationWithToken
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.security.web.util.matcher.RequestMatcher
import org.springframework.util.StringUtils
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

private val matchers by lazy {
    RequestMatcher{
        val login = AntPathRequestMatcher("/login")
        val wxLogin = AntPathRequestMatcher("/wxLogin")
        val error = AntPathRequestMatcher("/error")
        !(login.matches(it) || wxLogin.matches(it) || error.matches(it))
    }
}

class TokenFilter(authenticationManager: AuthenticationManager) : BasicFilter(matchers, authenticationManager) {

    override fun successfulAuthentication(request: HttpServletRequest,
                                           response: HttpServletResponse, chain: FilterChain, authResult: Authentication) {
        super.successfulAuthentication(request, response, chain, authResult)
        chain.doFilter(request, response)
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication? {
        val token = request.getParameter("token")
        if (StringUtils.isEmpty(token) || StringUtils.isEmpty(token)) {
            throw BadCredentialsException("无效的token")
        }
        val requestAuth = PreAuthenticationWithToken(token)
        return authenticationManager.authenticate(requestAuth)
    }
}