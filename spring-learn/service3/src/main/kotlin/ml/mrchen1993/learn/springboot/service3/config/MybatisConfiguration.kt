package ml.mrchen1993.learn.springboot.service3.config

import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class MybatisConfiguration{

    @Bean
    fun mybatisConfigurationCustomizer() = ConfigurationCustomizer {}
}