package ml.mrchen1993.learn.springboot.service2.config

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.boot.web.servlet.error.ErrorAttributes
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.context.request.WebRequest
import java.time.LocalDateTime

@Configuration
class CustomErrorConfig {
    @Bean
    fun errorAttributes(): ErrorAttributes {

        return object : DefaultErrorAttributes() {
            override fun getErrorAttributes(webRequest: WebRequest, includeStackTrace: Boolean): Map<String, Any?> {
                val error = getError(webRequest)
                return mapOf(
                        "timestamp" to LocalDateTime.now(),
                        "message" to error?.message,
                        "throwable" to error?.javaClass?.name
                )
            }
        }
    }
}