package ml.mrchen1993.learn.springboot.service2.utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule

val jsonMapper:ObjectMapper = ObjectMapper()
        .registerModule(JavaTimeModule())