package ml.mrchen1993.learn.springboot.service2.config.security.provider

import ml.mrchen1993.learn.springboot.service2.config.security.TokenService
import ml.mrchen1993.learn.springboot.service2.config.security.UserDetails
import ml.mrchen1993.learn.springboot.service2.config.security.token.PreAuthenticationWithUsernamePassword
import ml.mrchen1993.learn.springboot.service2.config.security.token.SuccessAuthentication
import ml.mrchen1993.learn.springboot.service2.data.UserRepository
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import org.springframework.util.DigestUtils
import org.springframework.util.StringUtils

@Component
class UsernamePasswordAuthenticationProvider (
        private val tokenService: TokenService,
        private val userRepository: UserRepository
): AuthenticationProvider {

    @Throws(AuthenticationException::class)
    override fun authenticate(authentication: Authentication): Authentication {
        val username = authentication.principal.toString()
        val password = authentication.credentials.toString()

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            throw BadCredentialsException("用户名或密码为空")
        }
        val user = userRepository.findByUserName(username)?:throw BadCredentialsException("用户不存在")
        if (user.password != DigestUtils.md5DigestAsHex(password.toByteArray())) {
            throw BadCredentialsException("密码不正确")
        }
        val authorities = AuthorityUtils.createAuthorityList("ROLE_USER")
        if (user.isTeacher) {
            authorities.add(SimpleGrantedAuthority("ROLE_TEACHER"))
        } else {
            authorities.add(SimpleGrantedAuthority("ROLE_STUDENT"))
        }

        val token = tokenService.generateNewToken()
        val userDetails = UserDetails(user = user)
        val successAuthentication = SuccessAuthentication(authorities, userDetails, token)
        tokenService.store(token, successAuthentication)
        return successAuthentication
    }

    override fun supports(authentication: Class<*>): Boolean {
        return  PreAuthenticationWithUsernamePassword::class.java == authentication
    }
}