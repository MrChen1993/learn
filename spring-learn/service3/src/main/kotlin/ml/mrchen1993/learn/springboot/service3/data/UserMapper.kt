package ml.mrchen1993.learn.springboot.service3.data

import ml.mrchen1993.learn.springboot.common.NoArg
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select

@NoArg
data class User(
        var id:Long? = null,
        var name:String,
        var age: Byte
)

@Mapper
interface UserMapper{

    @Select("select * from user")
    fun findAll():List<User>

    @Select("select * from user where name = #{name}")
    fun findByName(@Param("name") name:String):User

    @Insert("insert into user(name, age) values(#name, #age)")
    fun save(@Param("name") name:String,@Param("age") age:Byte)
}