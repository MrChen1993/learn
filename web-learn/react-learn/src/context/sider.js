import React from 'react';

const SiderContext = React.createContext();

export function withSider(Element) {
  return props => <SiderContext.Consumer>{
    ({ toggle, collapsed }) => <Element collapsed={collapsed} toggle={toggle} {...props} />
  }</SiderContext.Consumer>
}

export class SiderProvider extends React.Component {
  state = {
    collapsed: false
  }
  toggle = () => {
    this.setState({ collapsed: !this.state.collapsed });
  }
  render() {
    return <SiderContext.Provider value={{
      toggle: this.toggle,
      collapsed: this.state.collapsed
    }}>
      {this.props.children}
    </SiderContext.Provider>
  }
}
