package ml.mrchen1993.learn.springboot.service2.config.security

import ml.mrchen1993.learn.springboot.service2.config.security.filter.CodeFilter
import ml.mrchen1993.learn.springboot.service2.config.security.filter.TokenFilter
import ml.mrchen1993.learn.springboot.service2.config.security.filter.UsernamePasswordFilter
import ml.mrchen1993.learn.springboot.service2.config.security.provider.CodeAuthenticationProvider
import ml.mrchen1993.learn.springboot.service2.config.security.provider.TokenAuthenticationProvider
import ml.mrchen1993.learn.springboot.service2.config.security.provider.UsernamePasswordAuthenticationProvider
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

private const val ADMIN_ROLE = "admin"
private const val GATE_ROLE = "gate"

@Configuration
@EnableWebSecurity
class SecurityConfiguration(
        private val usernamePasswordAuthenticationProvider: UsernamePasswordAuthenticationProvider,
        private val codeAuthenticationProvider: CodeAuthenticationProvider,
        private val tokenAuthenticationProvider: TokenAuthenticationProvider
): WebSecurityConfigurerAdapter() {

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        val adminEndpoints = arrayOf("/admin")
        val gateEndpoints = arrayOf("/workspace")

        http
                .addFilterBefore(UsernamePasswordFilter(authenticationManager()), UsernamePasswordAuthenticationFilter::class.java)
                .addFilterBefore(CodeFilter(authenticationManager()), UsernamePasswordAuthenticationFilter::class.java)
                .addFilterBefore(TokenFilter(authenticationManager()), UsernamePasswordAuthenticationFilter::class.java)
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(*adminEndpoints).hasRole(ADMIN_ROLE)
                .antMatchers(*gateEndpoints).hasRole(GATE_ROLE)
                .and()
                .anonymous()
                .disable()
                .exceptionHandling()
                .accessDeniedHandler(AuthExceptionHandler)
                .authenticationEntryPoint(AuthExceptionHandler)

    }


    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth!!.authenticationProvider(usernamePasswordAuthenticationProvider)
                .authenticationProvider(codeAuthenticationProvider)
                .authenticationProvider(tokenAuthenticationProvider)
    }
}