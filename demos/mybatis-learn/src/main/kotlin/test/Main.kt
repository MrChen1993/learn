package test

import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.io.Resources
import org.apache.ibatis.session.SqlSessionFactoryBuilder

data class Blog(
        var id: Long? = null,
        var title:String? = null
)
@Mapper
interface BlogMapper{
    fun selectBlog(id: Long): Blog
}

fun main(args:Array<String>){
    val resource = "mybatis-config.xml"
    val inputStream = Resources.getResourceAsStream(resource)
    val sqlSessionFactory = SqlSessionFactoryBuilder().build(inputStream)

    val session = sqlSessionFactory.openSession()
    try {
        val mapper = session.getMapper(BlogMapper::class.java)
        val blog = mapper.selectBlog(101)
        println(blog.title)
    }finally {
        session.close()
    }
}