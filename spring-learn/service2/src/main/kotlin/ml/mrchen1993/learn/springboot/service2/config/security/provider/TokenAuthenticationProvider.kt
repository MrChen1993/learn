package ml.mrchen1993.learn.springboot.service2.config.security.provider

import ml.mrchen1993.learn.springboot.service2.config.security.TokenService
import ml.mrchen1993.learn.springboot.service2.config.security.token.PreAuthenticationWithToken
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils

@Component
class TokenAuthenticationProvider (
        private val tokenService: TokenService
): AuthenticationProvider {

    override fun authenticate(authentication: Authentication): Authentication {
        val token = authentication.credentials.toString()
        if (StringUtils.isEmpty(token)) {
            throw BadCredentialsException("无效的token")
        }
        if (!tokenService.contains(token)) {
            throw BadCredentialsException("无效的token或token已过期")
        }
        return tokenService.retrieve(token)?:throw BadCredentialsException("无效的token或token已过期")
    }

    override fun supports(authentication: Class<*>): Boolean {
        return PreAuthenticationWithToken::class.java == authentication
    }
}