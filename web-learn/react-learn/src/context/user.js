import React from 'react';

const UserContext = React.createContext();

export function withUser(Element) {
  return props => <UserContext.Consumer>{
    ({ user, login, logout }) => <Element user={user} login={login} logout={logout} {...props} />
  }</UserContext.Consumer>
}

export class UserProvider extends React.Component {
  state = {
    user: null
  }
  login = user => {
    localStorage.setItem("user", JSON.stringify(user))
    this.setState({ user });
  }
  logout = () => {
    localStorage.removeItem("user");
    this.setState({ user: null });
  }
  componentWillMount() {
    const userStr = localStorage.getItem("user");
    if (userStr) {
      const user = JSON.parse(userStr);
      this.setState({ user });
    }
  }
  render() {
    return <UserContext.Provider value={{
      user: this.state.user,
      login: this.login,
      logout: this.logout
    }}>{this.props.children}</UserContext.Provider>
  }
}
