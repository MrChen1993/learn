package ml.mrchen1993.learn.springboot.service2.config.security.provider

import ml.mrchen1993.learn.springboot.service2.config.security.TokenService
import ml.mrchen1993.learn.springboot.service2.config.security.UserDetails
import ml.mrchen1993.learn.springboot.service2.config.security.token.PreAuthenticationWithCode
import ml.mrchen1993.learn.springboot.service2.config.security.token.SuccessAuthentication
import ml.mrchen1993.learn.springboot.service2.data.UserRepository
import ml.mrchen1993.learn.springboot.service2.service.WxApiService
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils

@Component
class CodeAuthenticationProvider(
        private val tokenService: TokenService,
        private val wxApiService: WxApiService,
        private val userRepository: UserRepository
): AuthenticationProvider{
    override fun authenticate(authentication: Authentication?): Authentication {
        val code = authentication?.credentials?.toString()
        if (StringUtils.isEmpty(code)) {
            throw BadCredentialsException("无效的token")
        }
        val wxUser = wxApiService.login(code!!)?:throw BadCredentialsException("微信认证失败")
        val openId = wxUser.openId
        val user = userRepository.findByOpenId(openId)

        val authorities = AuthorityUtils.createAuthorityList("ROLE_wxUSER")
        if (user != null) {
            authorities.add(SimpleGrantedAuthority("ROLE_USER"))
            if (user.isAdmin) {
                authorities.add(SimpleGrantedAuthority("ROLE_ADMIN"))
            }
            if (user.isTeacher) {
                authorities.add(SimpleGrantedAuthority("ROLE_TEACHER"))
            } else {
                authorities.add(SimpleGrantedAuthority("ROLE_STUDENT"))
            }
        }

        val token = tokenService.generateNewToken()
        val userDetails = UserDetails(user, wxUser)
        val successAuthentication = SuccessAuthentication(authorities, userDetails, token)
        tokenService.store(token, successAuthentication)

        return successAuthentication
    }

    override fun supports(authentication: Class<*>?): Boolean {
        return PreAuthenticationWithCode::class.java == authentication
    }
}