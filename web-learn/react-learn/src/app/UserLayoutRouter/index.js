import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Layout } from 'antd';
import { Welcome } from '../../pages';
import AuthRoute from '../AuthRoute';
import MenuSider from './MenuSider';
import Footer from './Footer';
import Header from './Header';
import styled from 'styled-components';

const Page = styled.div`
  padding: 24px;
  background: #fff;
  min-height: 360px;
`

export default () => (
  <div>
    <Layout style={{ minHeight: '100vh' }}>
      <MenuSider />
      <Layout>
        <Header />
        <Layout.Content style={{ margin: '16px 16px' }}>
          <Page>
            <Switch>
              <Route exact path="/user" component={Welcome} />
              <Route path="/user/hello" component={() => (<div>你好</div>)} />
              <AuthRoute hasRole={["admin"]} path="/user/admin" component={() => (<div>你好</div>)} />
              <Route component={() => <Redirect to="/404" />} />
            </Switch>
          </Page>
        </Layout.Content>
        <Footer />
      </Layout>
    </Layout>
  </div>
)
