package ml.mrchen1993.learn.springboot.service2.utils

const val TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss"
const val DATE_PATTERN = "yyyy-MM-dd"