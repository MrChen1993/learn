package ml.mrchen1993.learn.springboot.service3.controller

import ml.mrchen1993.learn.springboot.service3.data.UserMapper
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("user")
class UserController(
        private val userMapper: UserMapper
){

    @GetMapping
    fun findByName() = userMapper.findAll()

    @PostMapping
    fun save(name:String, age:Byte){
        userMapper.save(name, age)
    }


}