package ml.mrchen1993.learn.springboot.service2.test

import ml.mrchen1993.learn.springboot.service2.data.User
import ml.mrchen1993.learn.springboot.service2.data.UserRepository
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@SpringBootTest
@RunWith(SpringRunner::class)
class TestWxApi (
        private val userRepository: UserRepository
){
    @Test
    fun testSave() {
        val user = User(userName = "陈龙", password = "川A12345")
        userRepository.save(user)
    }
}
