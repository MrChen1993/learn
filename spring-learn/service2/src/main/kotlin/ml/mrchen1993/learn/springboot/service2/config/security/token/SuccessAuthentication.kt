package ml.mrchen1993.learn.springboot.service2.config.security.token

import ml.mrchen1993.learn.springboot.service2.config.security.UserDetails
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class SuccessAuthentication(
        authorities: Collection<GrantedAuthority>,
        private val userDetails:UserDetails,
        val token:String
) :AbstractAuthenticationToken(authorities){

    init {
        details = token
        isAuthenticated = true
    }

    override fun getPrincipal() = userDetails
    override fun getCredentials() = null
}