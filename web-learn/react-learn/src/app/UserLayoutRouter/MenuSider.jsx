import React from 'react';
import {Menu, Icon, Layout} from 'antd';
import styled from 'styled-components';
import { withSider } from '../../context';

const Logo = styled.div`
  height: 32px;
  background: ${props => props.theme.main};
  margin: 16px;
`
export default
@withSider
class MenuSider extends React.Component {
  render() {
    const {collapsed} = this.props;
    return (
      <Layout.Sider
        collapsible
        collapsed={collapsed}
        trigger={null}
      >
        <Logo light/>
        <Menu theme="dark">
          <Menu.Item key="1">
            <Icon type="desktop" />
            <span>Option1</span>
          </Menu.Item>
        </Menu>
      </Layout.Sider>
    )
  }
}
