package ml.mrchen1993.learn.springboot.service2.service

import ml.mrchen1993.learn.springboot.service2.config.security.WxUser
import ml.mrchen1993.learn.springboot.service2.utils.jsonMapper
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.io.IOException

@Component
@ConfigurationProperties(prefix = "wx-api")
class WxApiProperties{
    lateinit var appId:String
    lateinit var appSecret:String
    lateinit var loginUrl:String
}

@Service
class WxApiService (
        private val wxApiProperties: WxApiProperties,
        private val restTemplate: RestTemplate
){
    private val logger = LoggerFactory.getLogger(this.javaClass)

    fun login(code: String): WxUser? {
        try {
            val loginUrl = wxApiProperties.loginUrl
            val appId = wxApiProperties.appId
            val appSecret = wxApiProperties.appSecret

            logger.debug(">>>>>>>>>>>>>>>>微信登录请求,code={}", code)
            val url = "$loginUrl?appid=$appId&secret=$appSecret&js_code=$code&grantType=authorization_code"

            val result = restTemplate.getForObject<String>(url, String::class.java)
            logger.debug("<<<<<<<<<<<<<<<<微信请求返回结果,result={}", result)

            val node = jsonMapper.readTree(result)

            val errorCode = node.get("errcode")
            if (errorCode != null) {
                logger.warn("微信登录失败,result={}", result)
            } else {
                val sessionKey = node.get("session_key").asText()
                val openId = node.get("openid").asText()
                if (sessionKey != null && openId != null) {
                    return WxUser(openId, sessionKey)
                } else {
                    logger.warn("微信返回的openId或sessionKey为空, result={}", result)
                }
            }
        } catch (e: RuntimeException) {
            logger.error("微信登录异常", e)
        } catch (e: IOException) {
            logger.error("json转换出错", e)
        }

        return null
    }
}