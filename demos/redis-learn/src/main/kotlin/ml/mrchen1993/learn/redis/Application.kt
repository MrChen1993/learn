package ml.mrchen1993.learn.redis

import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisPubSub

fun main(args:Array<String>){
    val jedis = Jedis("localhost", 6379)
    jedis.subscribe(object: JedisPubSub(){
        override fun onMessage(channel: String?, message: String?) {
            print(message)
        }
    },"hello")
    Thread.currentThread().join()
    println("Hello World")
}