import React from 'react';

import { withUser, withChangeTheme,withBreadcrumb } from '../context';
import { Link } from 'react-router-dom';
import { withTheme} from 'styled-components';


@withUser
@withTheme
@withChangeTheme
@withBreadcrumb([
  ["/","主页"]
])
export class Welcome extends React.Component{
  changeTheme = () =>{
    const {changeTheme, theme} = this.props;
    changeTheme({
      main: theme.main === 'black'?'mediumseagreen':'black'
    })
  }
  render () {
    const {user} = this.props;
    return (
      <div>
        <div>欢迎光临{user.name}</div>
        <button onClick={this.changeTheme}>更改主题</button>
        <Link to="/logout">退出</Link>
    </div>
    );
  }
}
