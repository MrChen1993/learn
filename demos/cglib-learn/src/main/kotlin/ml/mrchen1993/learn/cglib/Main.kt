package ml.mrchen1993.learn.cglib

import net.sf.cglib.proxy.Enhancer
import net.sf.cglib.proxy.MethodInterceptor
import net.sf.cglib.proxy.MethodProxy
import java.lang.reflect.Method

open class Hello(private val name:String?){
    open fun sayHello() = println("Hello:$name")
}

object CglibProxy:MethodInterceptor{

    inline fun <reified T> getProxyInstance():T{
        val enhancer = Enhancer()
        enhancer.setSuperclass(T::class.java)
        enhancer.setCallback(this)
        return enhancer.create(arrayOf(String::class.java), arrayOf("陈龙")) as T
    }

    override fun intercept(obj: Any, method: Method, args: Array<out Any>, proxy: MethodProxy): Any? {
        println("Before method:")
        val result = proxy.invokeSuper(obj, args)
        println("After method")
        return result
    }
}

fun main(){
    val hello:Hello = CglibProxy.getProxyInstance()
    hello.sayHello()
}