const { override,useEslintRc,fixBabelImports, addLessLoader,addBabelPlugin,addDecoratorsLegacy } = require('customize-cra');

module.exports = override(
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true,
    }),
    useEslintRc(),
    addBabelPlugin(["styled-components",{displayName: true}]),
    addDecoratorsLegacy(),
    addLessLoader({
        javascriptEnabled: true,
        modifyVars: { '@primary-color': '#1DA57A' },
    }),
);
