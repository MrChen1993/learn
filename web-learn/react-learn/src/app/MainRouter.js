import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import AuthRoute from './AuthRoute';
import { Home, Login, Register, Logout, Page403, Page404} from '../pages';
import UserLayoutRouter from './UserLayoutRouter';

export default () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/login" component={Login} />
    <Route path="/register" component={Register} />
    <AuthRoute path="/logout" component={Logout} />
    <AuthRoute hasRole={["user"]} path="/user" component={UserLayoutRouter} />
    <Route path="/403" component={Page403} />
    <Route path="/404" component={Page404} />
    <Route component={() => <Redirect to="/404" />} />
  </Switch>);
