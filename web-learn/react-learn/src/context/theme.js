import React from 'react';
import {ThemeProvider as StyledThemeProvider} from 'styled-components'

const ThemeContext = React.createContext();

export function withChangeTheme(Element) {
  return props => <ThemeContext.Consumer>{
    ({ changeTheme }) => <Element changeTheme={changeTheme} {...props} />
  }</ThemeContext.Consumer>
}

export class ThemeProvider extends React.Component {
  state = {
    theme: {
      main: 'mediumseagreen'
    }
  }
  changeTheme = (theme) => {
    this.setState({ theme });
  }
  render() {
    return <ThemeContext.Provider value={{
      changeTheme: this.changeTheme
    }}>
      <StyledThemeProvider theme={this.state.theme} >
        {this.props.children}
      </StyledThemeProvider>
    </ThemeContext.Provider>
  }
}
