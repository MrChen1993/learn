package ml.mrchen1993.learn.springboot.service2.data

import org.springframework.data.repository.CrudRepository
import javax.persistence.Entity

@Entity
data class User(
        var userName: String,
        var password: String,
        var openId:String? = null,
        var isTeacher: Boolean = false,
        var isAdmin:Boolean = false
): BaseEntity()

interface UserRepository: CrudRepository<User,Long>{
    fun findByOpenId(openId: String): User?
    fun findByUserName(userName:String): User?
}