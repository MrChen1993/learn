cmake_minimum_required(VERSION 3.9)
project(learn)

add_subdirectory(openssl-learn)
add_subdirectory(opencv-learn)
add_subdirectory(algorithm)

set(CMAKE_CXX_STANDARD 11)
