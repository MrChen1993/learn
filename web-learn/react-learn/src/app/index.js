import React from 'react';
import { BrowserRouter as RouterProvider} from 'react-router-dom';
import { UserProvider, ThemeProvider,SiderProvider,BreadcrumbProvider } from '../context';
import MainRouter from './MainRouter';

const Provider = ({children}) => (
  <UserProvider>
    <RouterProvider>
      <ThemeProvider>
        <SiderProvider>
          <BreadcrumbProvider>
            {children}
          </BreadcrumbProvider>
        </SiderProvider>
      </ThemeProvider>
    </RouterProvider>
  </UserProvider>
)


export default () => (
  <Provider>
    <MainRouter />
  </Provider>
)
