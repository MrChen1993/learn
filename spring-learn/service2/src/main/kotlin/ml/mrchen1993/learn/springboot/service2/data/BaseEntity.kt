package ml.mrchen1993.learn.springboot.service2.data

import com.fasterxml.jackson.annotation.JsonFormat
import ml.mrchen1993.learn.springboot.service2.utils.TIMESTAMP_PATTERN
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.*

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
abstract class BaseEntity(
        @Id
        @GeneratedValue
        val id:Long? = null,
        @CreatedDate
        @JsonFormat(pattern = TIMESTAMP_PATTERN)
        val createTime: LocalDateTime? = null,
        @CreatedBy
        val createUser: String? = null,
        @LastModifiedDate
        @JsonFormat(pattern = TIMESTAMP_PATTERN)
        val updateTime: LocalDateTime? = null,
        @LastModifiedBy
        val updateUser: String? = null,
        @Version
        val version: String? = null
)