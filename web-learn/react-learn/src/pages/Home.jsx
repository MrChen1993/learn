import React from 'react';
import { withUser } from '../context';
import { Redirect } from 'react-router-dom';

export const Home = withUser(({ user }) => {
  if (user) {
    return <Redirect to="/user" />
  } else {
    return <Redirect to="/login" />
  }
});
