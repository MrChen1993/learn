package ml.mrchen1993.learn.springboot.service2.config.security

import ml.mrchen1993.learn.springboot.service2.config.security.token.SuccessAuthentication
import net.sf.ehcache.CacheManager
import net.sf.ehcache.Element
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.*

@Component
class TokenService{

    private val cache by lazy {
        CacheManager.getInstance().getCache("tokenCache")
    }

    @Scheduled(fixedRate = 30 * 60 * 1000)
    fun evictExpiredTokens(){
        cache.evictExpiredElements()
    }
    fun generateNewToken() = UUID.randomUUID().toString()
    fun store(token:String, authentication: SuccessAuthentication){
        cache.put(Element(token, authentication))
    }
    fun contains(token:String) = cache[token] != null
    fun retrieve(token:String) = cache[token].objectValue as SuccessAuthentication?
    fun remove(token: String) = cache.remove(token)
}