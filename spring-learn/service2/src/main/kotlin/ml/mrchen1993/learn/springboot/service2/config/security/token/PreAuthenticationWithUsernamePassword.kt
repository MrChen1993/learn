package ml.mrchen1993.learn.springboot.service2.config.security.token

import org.springframework.security.authentication.AbstractAuthenticationToken
import java.util.*

class PreAuthenticationWithUsernamePassword(
        private val username:String,
        private val password:String
): AbstractAuthenticationToken(Collections.emptyList()){

    override fun getCredentials() = password
    override fun getPrincipal() = username
}