package ml.mrchen1993.learn.springboot.service3

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Service3Application

fun main(){
    runApplication<Service3Application>()
}