//
// Created by chenlong on 2018/6/5.
//

void insert_sort(int array[], int length) {
    for (int i = 1; i <= length; i++) {
        auto key = array[i];
        int j = i - 1;
        while (j > 0 && array[j] > key) {
            array[j + 1] = array[j];
            j = j - 1;
        }
        array[i + 1] = key;
    }
}