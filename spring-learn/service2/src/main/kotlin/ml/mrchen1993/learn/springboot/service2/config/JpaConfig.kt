package ml.mrchen1993.learn.springboot.service2.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.domain.AuditorAware
import org.springframework.data.jpa.repository.config.EnableJpaAuditing
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import java.util.*

@Configuration
@EnableJpaAuditing
class JpaConfig {

    @Bean
    fun auditorAware(): AuditorAware<String> {
        return  AuditorAware{
            val authentication:Authentication? = SecurityContextHolder.getContext().authentication
            Optional.ofNullable(authentication).map(Authentication::getPrincipal).map(Any::toString)
        }
    }
}