package ml.mrchen1993.learn.springboot.service2.config.security.token

import org.springframework.security.authentication.AbstractAuthenticationToken
import java.util.*

class PreAuthenticationWithCode(private val code:String):AbstractAuthenticationToken(Collections.emptyList()){

    override fun getCredentials() = code
    override fun getPrincipal() = null
}