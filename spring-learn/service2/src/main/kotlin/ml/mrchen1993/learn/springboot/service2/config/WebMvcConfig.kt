package ml.mrchen1993.learn.springboot.service2.config

import ml.mrchen1993.learn.springboot.service2.interceptor.TimeInterceptor
import ml.mrchen1993.learn.springboot.service2.utils.jsonMapper
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
@EnableWebMvc
class WebMvcConfig:WebMvcConfigurer{
    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        converters.add(MappingJackson2HttpMessageConverter(jsonMapper))
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(TimeInterceptor()).addPathPatterns("/**")
    }
}