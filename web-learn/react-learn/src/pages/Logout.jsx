import React from 'react';
import { withUser } from '../context';
import { withRouter } from 'react-router';
class LogoutPage extends React.Component {
  componentWillMount() {
    const { logout, history } = this.props;
    logout();
    history.push("/");
  }
  render() {
    return null
  }
}

export const Logout = withRouter(withUser(
  ({ logout, history }) => (<LogoutPage logout={logout} history={history} />)
));
