package ml.mrchen1993.learn.springboot.service2.config.security

import ml.mrchen1993.learn.springboot.service2.utils.jsonMapper
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.security.web.access.AccessDeniedHandler
import org.springframework.security.web.authentication.AuthenticationFailureHandler
import java.time.LocalDateTime
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

object AuthExceptionHandler:AuthenticationEntryPoint, AccessDeniedHandler, AuthenticationFailureHandler{

    private fun service(response:HttpServletResponse, exception:Exception?){
        response.status = when(exception){
            is AuthenticationException-> HttpServletResponse.SC_UNAUTHORIZED
            is AccessDeniedException->HttpServletResponse.SC_FORBIDDEN
            else->HttpServletResponse.SC_INTERNAL_SERVER_ERROR
        }
        response.contentType = "application/json"
        response.characterEncoding = "UTF-8"

        val errorAttributes = mapOf(
                "timestamp" to LocalDateTime.now(),
                "message" to exception?.message,
                "throwable" to exception?.javaClass?.name
        )
        val result = jsonMapper.writeValueAsString(errorAttributes)
        response.writer.write(result)

    }

    override fun commence(request: HttpServletRequest, response: HttpServletResponse, authException: AuthenticationException?) {
        service(response, authException)
    }

    override fun handle(request: HttpServletRequest, response: HttpServletResponse, accessDeniedException: AccessDeniedException?) {
        service(response, accessDeniedException)

    }

    override fun onAuthenticationFailure(request: HttpServletRequest, response: HttpServletResponse, exception: AuthenticationException?) {
        service(response, exception)

    }
}