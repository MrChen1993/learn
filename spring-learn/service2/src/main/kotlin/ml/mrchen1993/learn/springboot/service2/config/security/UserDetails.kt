package ml.mrchen1993.learn.springboot.service2.config.security

import ml.mrchen1993.learn.springboot.service2.data.User

class WxUser(
        var openId:String,
        var sessionKey:String
)

class UserDetails(
        var user: User? = null,
        var wxUser:WxUser? = null
)