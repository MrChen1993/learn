import React from 'react';
import { Redirect, Route} from 'react-router-dom';
import { withUser } from '../context';

export default withUser(function ({user, hasRole=[], ...props}){
  if (!user) {
    return <Redirect to="/login" />
  }

  if (hasRole.find(role => !(user.roles || []).includes(role))) {
    return <Redirect to={"/403"} />
  }

  return <Route {...props} />
})
