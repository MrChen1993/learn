package ml.mrchen1993.learn.springboot.service2.config.security.filter

import ml.mrchen1993.learn.springboot.service2.config.security.token.PreAuthenticationWithCode
import ml.mrchen1993.learn.springboot.service2.config.security.token.SuccessAuthentication
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.util.StringUtils
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class CodeFilter (authenticationManager:AuthenticationManager):
        BasicFilter(AntPathRequestMatcher("/wxLogin"), authenticationManager){

    init {
        setAuthenticationSuccessHandler(successHandler())
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication? {
        val code = request.getParameter("code")
        if(StringUtils.isEmpty(code)){
            throw BadCredentialsException("微信登录code为空")
        }
        val requestAuth = PreAuthenticationWithCode(code)
        return authenticationManager.authenticate(requestAuth)
    }

    private fun successHandler(): AuthenticationSuccessHandler {
        return AuthenticationSuccessHandler{
            _, response, authentication ->
            if (authentication is SuccessAuthentication) {
                response.writer.println(authentication.token)
            }
        }
    }
}