package ml.mrchen1993.learn.springboot.service2.converter

import ml.mrchen1993.learn.springboot.service2.utils.TIMESTAMP_PATTERN
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.persistence.AttributeConverter
import javax.persistence.Converter as JPAConverter

@JPAConverter
class LocalDateTime2Date : AttributeConverter<LocalDateTime, Timestamp> {
    override fun convertToDatabaseColumn(attribute: LocalDateTime?): Timestamp? {
        return if (attribute != null) Timestamp.valueOf(attribute) else null
    }

    override fun convertToEntityAttribute(dbData: Timestamp?): LocalDateTime? {
        return dbData?.toLocalDateTime()
    }
}

@Component
class String2LocalDateTime : Converter<String, LocalDateTime> {
    override fun convert(source: String): LocalDateTime? {
        if (StringUtils.isEmpty(source)) {
            return null
        }
        val formatter = DateTimeFormatter.ofPattern(TIMESTAMP_PATTERN)
        return LocalDateTime.parse(source, formatter)
    }
}
