package ml.mrchen1993.learn.springboot.service2.config.security.filter

import ml.mrchen1993.learn.springboot.service2.config.security.token.PreAuthenticationWithUsernamePassword
import ml.mrchen1993.learn.springboot.service2.config.security.token.SuccessAuthentication
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.util.StringUtils
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

 class UsernamePasswordFilter(
         authenticationManager: AuthenticationManager
 ) : BasicFilter (AntPathRequestMatcher("/login"), authenticationManager) {

     init {
         setAuthenticationSuccessHandler(successHandler())
     }

     override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
         val username = request.getParameter("username")
         val password = request.getParameter("password")
         if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
             throw BadCredentialsException("用户名和密码不能为空")
         }
         val requestAuth = PreAuthenticationWithUsernamePassword(username, password)
         return authenticationManager.authenticate(requestAuth)
     }

     private fun successHandler(): AuthenticationSuccessHandler {
         return AuthenticationSuccessHandler { _, response, authentication ->
             if (authentication is SuccessAuthentication) {
                 response.writer.println(authentication.token)
             }
         }
     }
 }