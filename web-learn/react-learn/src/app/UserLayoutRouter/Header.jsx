import React from 'react';
import {Layout, Icon, Breadcrumb} from 'antd';
import { withSider,withBreadcrumb } from '../../context';
import styled from 'styled-components';
import {Link } from 'react-router-dom';

const StyledHeader = styled(Layout.Header)`
  background: #fff !important;
  padding: 0;
  display: flex;
  align-items: center;
`

const StyledIcon = styled(Icon)`
  font-size: 18px;
  line-height: 64px;
  padding: 0 24px;
  cursor: pointer;
  transition: color .3s;
  &:hover {
    color: #1890ff;
  }
`

export default
@withSider
@withBreadcrumb()
class extends React.Component{
  render () {
    const {collapsed, toggle, breadcrumbs} = this.props;
    return (
      <StyledHeader>
        <StyledIcon
          type={collapsed ? 'menu-unfold' : 'menu-fold'}
          onClick={toggle}
        />
        <Breadcrumb style={{ margin: '16px 0' }}>
          {
            breadcrumbs.map(item=><Breadcrumb.Item key={item[0]}><Link to={item[0]}>{item[1]}</Link></Breadcrumb.Item>)
          }
        </Breadcrumb>
      </StyledHeader>
    );
  }
}
