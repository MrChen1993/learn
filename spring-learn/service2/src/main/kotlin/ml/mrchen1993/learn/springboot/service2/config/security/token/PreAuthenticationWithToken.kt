package ml.mrchen1993.learn.springboot.service2.config.security.token

import org.springframework.security.authentication.AbstractAuthenticationToken
import java.util.*

class PreAuthenticationWithToken(private val token:String): AbstractAuthenticationToken(Collections.emptyList()){

    override fun getCredentials() = token
    override fun getPrincipal() = null
}