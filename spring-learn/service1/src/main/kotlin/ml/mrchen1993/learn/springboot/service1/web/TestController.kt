package ml.mrchen1993.learn.springboot.service1.web

import org.springframework.beans.factory.annotation.Value
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RefreshScope
@RestController
class TestController{

    @Value("\${message:Hello default}")
    private lateinit var test:String

    @GetMapping("test")
    fun test() = test
}