package ml.mrchen1993.learn.springboot.service2.interceptor

import org.slf4j.LoggerFactory
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.ModelAndView
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

private const val TIME_FILED_NAME = "controller.start.time"

class TimeInterceptor : HandlerInterceptor {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        request.setAttribute(TIME_FILED_NAME, System.currentTimeMillis())
        return true
    }

    override fun postHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any, modelAndView: ModelAndView?) {
        val startTime = request.getAttribute(TIME_FILED_NAME) as Long
        val endTime = System.currentTimeMillis()
        val executeTime = endTime - startTime
        logger.debug("{} 执行时间 {}", request.requestURI, executeTime)
    }

    override fun afterCompletion(request: HttpServletRequest, response: HttpServletResponse, handler: Any, ex: Exception?) {
    }
}