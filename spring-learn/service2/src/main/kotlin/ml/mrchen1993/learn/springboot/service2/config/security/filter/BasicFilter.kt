package ml.mrchen1993.learn.springboot.service2.config.security.filter

import ml.mrchen1993.learn.springboot.service2.config.security.AuthExceptionHandler
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.security.web.util.matcher.RequestMatcher

abstract class BasicFilter(requestMatcher: RequestMatcher, authenticationManager: AuthenticationManager)
    : AbstractAuthenticationProcessingFilter(requestMatcher){
    init {
        this.authenticationManager = authenticationManager
        this.setAuthenticationFailureHandler(AuthExceptionHandler)
        this.setAuthenticationSuccessHandler(successHandler())
        this.setContinueChainBeforeSuccessfulAuthentication(false)
    }

    private fun successHandler(): AuthenticationSuccessHandler{
        return AuthenticationSuccessHandler{
            _, _, _ ->
        }
    }
}