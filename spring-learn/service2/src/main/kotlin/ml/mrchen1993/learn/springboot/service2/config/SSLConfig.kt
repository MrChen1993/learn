package ml.mrchen1993.learn.springboot.service2.config

import org.apache.catalina.Context
import org.apache.catalina.connector.Connector
import org.apache.tomcat.util.descriptor.web.SecurityCollection
import org.apache.tomcat.util.descriptor.web.SecurityConstraint
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SSLConfig {

    @Value("\${server.http.port:}")
    internal var httpPort: Int? = null

    //正常启用的https端口 如443
    @Value("\${server.port}")
    internal var httpsPort: Int? = null

    @Bean
    fun connector(): Connector {
        //80端口自动转443端口实现http访问时候https地址
        val connector = Connector("org.apache.coyote.http11.Http11NioProtocol")
        connector.scheme = "http"
        connector.port = httpPort!!
        connector.secure = false
        connector.redirectPort = httpsPort!!
        return connector
    }

    @Bean
    fun tomcatServletWebServerFactory(connector: Connector): TomcatServletWebServerFactory {

        val tomcat = object : TomcatServletWebServerFactory() {
            override fun postProcessContext(context: Context) {
                val securityConstraint = SecurityConstraint()
                securityConstraint.userConstraint = "CONFIDENTIAL"
                val collection = SecurityCollection()
                collection.addPattern("/*")
                securityConstraint.addCollection(collection)
                context.addConstraint(securityConstraint)
            }
        }
        tomcat.addAdditionalTomcatConnectors(connector)
        return tomcat
    }
}