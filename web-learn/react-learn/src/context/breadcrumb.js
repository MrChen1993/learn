import React from 'react';

const BreadcrumbContext = React.createContext();

function async(block){
  setTimeout(block, 0)
}

export function withBreadcrumb(newBreadcrumbs) {
  return function(Element) {
    return props => <BreadcrumbContext.Consumer>{
      ({ breadcrumbs, setBreadcrumbs }) => {
        async(()=>{
          if(newBreadcrumbs && breadcrumbs !== newBreadcrumbs){
            setBreadcrumbs(newBreadcrumbs);
          }
        })
        return <Element breadcrumbs={breadcrumbs} {...props} />
      }
    }</BreadcrumbContext.Consumer>
  }
}

export class BreadcrumbProvider extends React.Component {
  state = {
    breadcrumbs: []
  }
  setBreadcrumbs = (breadcrumbs) => {
    this.setState({ breadcrumbs });
  }
  render() {
    return <BreadcrumbContext.Provider value={{
      breadcrumbs: this.state.breadcrumbs,
      setBreadcrumbs: this.setBreadcrumbs
    }}>
      {this.props.children}
    </BreadcrumbContext.Provider>
  }
}
